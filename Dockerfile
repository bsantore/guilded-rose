FROM artifactory.kbra.com/docker/php:7.4
RUN apt update && apt install -y \
    git
COPY --from=artifactory.kbra.com/docker/composer:1.10.25 /usr/bin/composer /usr/bin/composer